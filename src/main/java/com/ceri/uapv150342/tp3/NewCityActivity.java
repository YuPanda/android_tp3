package com.ceri.uapv150342.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City city = new City(String.valueOf(textName.getText()),String.valueOf(textCountry.getText()));
                final WeatherDbHelper dbHelper=new WeatherDbHelper(NewCityActivity.this);
                dbHelper.addCity(city);
                Toast.makeText(NewCityActivity.this,"Rajout ville",Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }


}
