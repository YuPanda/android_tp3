package com.ceri.uapv150342.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    public WeatherDbHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,NewCityActivity.class);
                startActivity(intent);
            }
        });


        this.dbHelper=new WeatherDbHelper(this);
        this.dbHelper.populate();


        SimpleCursorAdapter cursorAdapter=new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String []{WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1,android.R.id.text2});
        final ListView listView = findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor curseCurrent = (Cursor)parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG,dbHelper.cursorToCity(curseCurrent));
                startActivity(intent);
                onResume();
            }
        });
        final SwipeRefreshLayout swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh() {
                allUpdate all = new allUpdate(dbHelper.getAllCities(),dbHelper, swipeRefreshLayout);
                all.execute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        SimpleCursorAdapter cursorAdapter=new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                this.dbHelper.fetchAllCities(),
                new String []{WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1,android.R.id.text2});
        final ListView listView = findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
    }

}
