package com.ceri.uapv150342.tp3;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class allUpdate extends AsyncTask<Void,Void,Void>
{
    private List<City> city;
    private WeatherDbHelper weather;
    private SwipeRefreshLayout swipeRefreshLayout;

    public allUpdate(List<City> city, WeatherDbHelper weather, SwipeRefreshLayout swipeRefreshLayout) {
        this.city = city;
        this.weather = weather;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        URL url = null;
        HttpURLConnection urlConnection;
        for(int i = 0; i < city.size(); i++) {
            try {
                url = WebServiceUrl.build(city.get(i).getName(),city.get(i).getCountry());
                try
                {
                    urlConnection = (HttpURLConnection) url.openConnection();
                    JSONResponseHandler json = new JSONResponseHandler(city.get(i));
                    urlConnection.connect();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    json.readJsonStream(in);
                    urlConnection.disconnect();
                    weather.updateCity(city.get(i));
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        this.swipeRefreshLayout.setRefreshing(false);
        return null;
    }

}

